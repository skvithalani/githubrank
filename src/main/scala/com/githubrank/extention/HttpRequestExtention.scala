package com.githubrank.extention

import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.headers.{Authorization, GenericHttpCredentials}

object HttpRequestExtention {

  implicit class RichHttpRequest(val httpRequest: HttpRequest) extends AnyVal {
    def addOptionalToken: HttpRequest = {
      val token = sys.env.get("GH_TOKEN").map(t => Authorization(GenericHttpCredentials("token", t)))
      if (token.isDefined) httpRequest.addHeader(token.get) else httpRequest
    }
  }

}
