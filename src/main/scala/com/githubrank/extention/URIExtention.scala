package com.githubrank.extention

import akka.http.scaladsl.model.Uri.Path

object URIExtention {

  implicit class RichURI(val uri: Path) extends AnyVal {
    def toOrganization: String = {
      uri.toString.replace("/", "")
    }
  }

}
