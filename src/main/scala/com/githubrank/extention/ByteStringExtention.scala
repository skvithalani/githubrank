package com.githubrank.extention

import akka.util.ByteString
import play.api.libs.json.{JsObject, JsValue, Json}

import scala.collection.Map

object ByteStringExtention {

  implicit class RichByteString(val bs: ByteString) extends AnyVal {
    def toJSON: Map[String, JsValue] = Json.parse(bs.toArray).asInstanceOf[JsObject].value
  }

}
