package com.githubrank.http

import akka.actor.ActorSystem
import akka.http.caching.scaladsl.Cache
import akka.http.scaladsl.Http
import akka.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import akka.http.scaladsl.model.{HttpMethods, Uri, _}
import akka.http.scaladsl.server.Directives.{complete, extractUnmatchedPath, get, pathPrefix, pathSuffix, _}
import akka.http.scaladsl.server.directives.CachingDirectives
import akka.http.scaladsl.server.directives.CachingDirectives.routeCache
import akka.http.scaladsl.server.{RequestContext, Route, RouteResult}
import akka.stream.scaladsl.Source
import com.githubrank.extention.ByteStringExtention.RichByteString
import com.githubrank.extention.HttpRequestExtention.RichHttpRequest
import com.githubrank.extention.URIExtention.RichURI

import scala.async.Async.{async, await}
import scala.concurrent.{ExecutionContextExecutor, Future}

class GithubRankRoute(implicit val system: ActorSystem) {
  implicit val executionContext: ExecutionContextExecutor       = system.dispatcher
  implicit val jsonStreamingSupport: JsonEntityStreamingSupport = EntityStreamingSupport.json()

  var finalResult = Map.empty[String, Int]

  lazy val myCache: Cache[Uri, RouteResult] = routeCache[Uri]
  lazy val simpleKeyer: PartialFunction[RequestContext, Uri] = {
    case r: RequestContext if r.request.method == HttpMethods.GET => r.request.uri
  }

  def route(): Route = get {
    pathPrefix("org") {
      pathSuffix("contributors") {
        extractUnmatchedPath { remaining =>
          CachingDirectives.alwaysCache(myCache, simpleKeyer) {
            val organization = remaining.toOrganization
            complete {
              val resultF: Future[List[(String, Int)]] = async {
                val repos            = await(fetchOrgRepos(organization))
                val calculatedReposF = repos.map(calculateEachRepoContributors(organization, _))
                await(Future.sequence(calculatedReposF))
                finalResult.toList.sortWith(_._2 > _._2)
              }
              formJsonResponse(resultF)
            }
          }
        }
      }
    }
  }

  private def fetchOrgRepos(organization: String): Future[List[String]] = {
    val repoRequest = HttpRequest(uri = s"https://api.github.com/orgs/$organization/repos")
    val reposF: Future[List[String]] = Http()
      .singleRequest(repoRequest.addOptionalToken)
      .flatMap { response =>
        if (response.status == StatusCodes.OK) {
          response.entity.dataBytes.via(jsonStreamingSupport.framingDecoder).runFold[List[String]](List.empty) { (ls, bs) =>
            val repo = bs.toJSON("name").as[String]
            repo :: ls
          }
        }
        else Future.successful(List.empty[String])
      }
    reposF
  }

  private def calculateEachRepoContributors(organization: String, repo: String) = {
    async {
      val contributorRequest = HttpRequest(uri = s"https://api.github.com/repos/$organization/$repo/contributors")
      val response           = await(Http().singleRequest(contributorRequest.addOptionalToken))
      if (response.status == StatusCodes.OK) {
        val eventualDone = response.entity.dataBytes.via(jsonStreamingSupport.framingDecoder).runForeach { bs =>
          val data  = bs.toJSON
          val login = data("login").as[String]
          finalResult = finalResult.updated(login, finalResult.getOrElse(login, 0) + data("contributions").as[Int])
        }
        await(eventualDone)
      }
      else finalResult
    }
  }

  private def formJsonResponse(resultF: Future[List[(String, Int)]]): Future[HttpResponse] = {
    Source
      .future(resultF)
      .mapConcat(identity)
      .runFold("[") { (res, ll) =>
        res + s"""{"name":${ll._1},"contributions":${ll._2}},\n"""
      }
      .map(s => s"${s.stripTrailing().stripSuffix(",")}]")
      .map(x => HttpResponse(status = StatusCodes.OK, entity = HttpEntity(ContentTypes.`application/json`, x)))
  }
}
