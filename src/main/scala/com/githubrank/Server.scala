package com.githubrank

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import com.githubrank.http.GithubRankRoute

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.io.StdIn

object Server extends App {
  implicit val system: ActorSystem                        = ActorSystem("Server")
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val route: Route = new GithubRankRoute().route()

  val bindingFuture: Future[Http.ServerBinding] = Http().bindAndHandle(route, "localhost", 8080)

  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine()

  bindingFuture.flatMap(_.unbind()).onComplete { _ =>
    system.terminate()
  }
}
