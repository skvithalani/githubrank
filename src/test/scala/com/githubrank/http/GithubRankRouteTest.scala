package com.githubrank.http

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.Eventually
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import scala.language.postfixOps

import scala.concurrent.duration.DurationDouble

class GithubRankRouteTest extends AnyWordSpec with Matchers with ScalatestRouteTest with BeforeAndAfterAll {
  val route: Route = new GithubRankRoute().route()

  "The server" should {
    "handle org/:org/contributors route" in {
      Eventually.eventually(Eventually.timeout(20 seconds), Eventually.interval(1 second)) {
        Get("/org/skvithalani-test/contributors") ~> route ~> check {
          response.status shouldBe StatusCodes.OK
        }
      }
    }

    "not handle GET requests to paths other than contributors" in {
      Get("/org/skvithalani-test/insights") ~> route ~> check {
        handled shouldBe false
      }
    }

    "return user and contributions in json format" in {
      Eventually.eventually(Eventually.timeout(20 seconds), Eventually.interval(1 second)) {
        Get("/org/skvithalani-test/contributors") ~> route ~> check {
          responseAs[String] shouldEqual """[{"name":skvithalani,"contributions":1}]"""
        }
      }
    }

  }
}
