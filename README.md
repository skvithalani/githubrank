## GithubRank Challenge

### Problem statement

GitHub portal is centered around organizations and repositories. Each organization has
many repositories and each repository has many contributors. Your goal is to create an
endpoint that given the name of the organization will return a list of contributors sorted by the
number of contributions.
The endpoint should:
-  use GitHub REST API v3 (https://developer.github.com/v3/)
- return a list sorted by the number of contributions made by developer to all
repositories for the given organization.
- respond to a GET request at port 8080 and address /org/{org_name}/contributors
- respond with JSON, which should be a list of contributors in the following format:
{ “name”: <contributor_login>, “contributions”: <no_of_contributions> }
- handle GitHub’s API rate limit restriction using token that can be set as an
environment variable of name GH_TOKEN
- friendly hint: GitHub API returns paginated responses. You should take it into
account if you want the result to be accurate for larger organizations.

### How to run

- Set `GH_TOKEN` environment variable to authorize github apis and increase rate limit
- Run `Server.scala`
- Hit `http://localhost:8080/org/:org_name/contributors` from browser
- or `curl http://localhost:8080/org/:org_name/contributors` from terminal

The `org_name` should be replaced with valid organization.

#### Explanation 

- The server currently serves only singe route of the format `/org/:org_name/contributors`
- It fetches all repos for an organization, the for each repo fetches all contributors and finally
 merge them based on their contributions in descending order
- Github urls used in the code are `https://api.github.com/orgs/:organization/repos`
and `https://api.github.com/repos/:organization/:repo/contributors`
- Caching is implemented based on URI for e.g. for the same organization the data will cached
- Github personal token is used to authenticate the rate limiting apis of github and 
increasing the rate limit

#### Tech stack

- I have used `Akka Http` for implementing Http server and `Scalatest` runner for writing tests.
The functional tech-stack in market that could be used are`Http4s` along with `Circe`
but since I lack hands-on experience for the same I decided to use `Akka Http`
- Akka Http provides full-fledged DSL for writing routes, matching paths, caching responses, managing headers, etc.
- It gives first-class support for non-blocking asynchronous implementation as well

