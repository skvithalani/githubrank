name := "githubrank"

version := "0.1"

scalaVersion := "2.13.1"

val akkaVersion      = "2.6.4"
val httpVersion      = "10.1.11"
val scalaTestVersion = "3.1.1"

libraryDependencies += "com.typesafe.akka"      %% "akka-testkit"        % akkaVersion
libraryDependencies += "com.typesafe.akka"      %% "akka-stream-testkit" % akkaVersion
libraryDependencies += "com.typesafe.akka"      %% "akka-stream"         % akkaVersion
libraryDependencies += "com.typesafe.akka"      %% "akka-http"           % httpVersion
libraryDependencies += "com.typesafe.akka"      %% "akka-http-caching"   % httpVersion
libraryDependencies += "com.typesafe.akka"      %% "akka-http-testkit"   % httpVersion
libraryDependencies += "org.scala-lang"         % "scala-reflect"        % scalaVersion.value % Provided
libraryDependencies += "org.scalactic"          %% "scalactic"           % scalaTestVersion
libraryDependencies += "org.scalatest"          %% "scalatest"           % scalaTestVersion % "test"
libraryDependencies += "org.scala-lang.modules" %% "scala-async"         % "0.10.0"
libraryDependencies += "com.typesafe.play"      %% "play-json"           % "2.8.1"
